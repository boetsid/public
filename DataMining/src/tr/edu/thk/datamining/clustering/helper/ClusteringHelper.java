package tr.edu.thk.datamining.clustering.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.SwingWorker;

import tr.edu.thk.datamining.optics.helper.Util.PointComparisonType;
import tr.edu.thk.datamining.optics.helper.Util.PointType;
import tr.edu.thk.datamining.optics.model.Point;
import tr.edu.thk.datamining.optics.ui.MainFrameListener;

public abstract class ClusteringHelper extends SwingWorker<Void, Void> {
	protected List<Point>		points;

	protected Point				startingPoint;

	protected int				epsilon;

	protected int				minPts;

	protected List<Point>		reachabilityPlot;

	protected List<Point>		tempList;

	protected MainFrameListener	mainFrameListener;

	public ClusteringHelper(List<Point> points, Point startingPoint, int epsilon, int minPts) {
		this.points = points;
		this.startingPoint = startingPoint;
		this.epsilon = epsilon;
		this.minPts = minPts;

		reachabilityPlot = new ArrayList<Point>();

		tempList = new ArrayList<Point>();
		tempList.addAll(points);
	}
	
	protected abstract Void doInBackground() throws Exception;

	protected List<Point> getNeighborPoints(List<Point> points, Point centerPoint) {
		List<Point> neighborPoints = new ArrayList<Point>();

		for (Point point : points) {
			if (point == centerPoint) {
				continue;
			}

			double distance = centerPoint.distance(point);
			if (distance <= epsilon) {
				neighborPoints.add(point);
			}
		}

		return neighborPoints;
	}

	protected Double computeCoreDistance(Point point) {
		Comparator<Point> comparator = new PointComparator(PointComparisonType.DISTANCE, point);
		Collections.sort(tempList, comparator);

		if (tempList.size() <= minPts) {
			return null;
		}

		Double coreDistance = tempList.get(minPts).distance(point);

		return coreDistance;
	}

	protected void prepareForClustering() {
		for (Point point : points) {
			point.setType(PointType.UNDEFINED);
			point.setCoreDistance(null);
			point.setReachabilityDistance(null);
		}
	}

	public List<Point> getReachabilityPlot() {
		return reachabilityPlot;
	}

	public void setMainFrameListener(MainFrameListener mainFrameListener) {
		this.mainFrameListener = mainFrameListener;
	}
}
