package tr.edu.thk.datamining.clustering.helper;

import java.util.Comparator;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointComparisonType;
import tr.edu.thk.datamining.optics.model.Point;

public class PointComparator implements Comparator<Point> {
	private PointComparisonType	comparisonType;
	private Point				startingPoint;

	public PointComparator(PointComparisonType comparisonType) {
		this(comparisonType, null);
	}

	public PointComparator(PointComparisonType comparisonType, Point startingPoint) {
		this.startingPoint = startingPoint;
		this.comparisonType = comparisonType;
	}

	@Override
	public int compare(Point o1, Point o2) {
		switch (comparisonType) {
		case DISTANCE:
			return compareDistance(o1, o2);

		case CORE_DISTANCE:
			return compareCoreDistance(o1, o2);

		case REACHABILITY_DISTANCE:
			return compareReachabilityDistance(o1, o2);
		}

		return 0;
	}

	private int compare(double d1, double d2) {
		if (Util.equal(d1, d2)) {
			return 0;
		} else if (d1 < d2) {
			return -1;
		}

		return 1;
	}

	public int compareDistance(Point o1, Point o2) {
		double d1 = o1.distance(startingPoint);
		double d2 = o2.distance(startingPoint);

		return compare(d1, d2);
	}

	public int compareCoreDistance(Point o1, Point o2) {
		double d1 = o1.getCoreDistance();
		double d2 = o2.getCoreDistance();

		return compare(d1, d2);
	}

	public int compareReachabilityDistance(Point o1, Point o2) {
		double d1 = o1.getReachabilityDistance();
		double d2 = o2.getReachabilityDistance();

		return compare(d1, d2);
	}
}
