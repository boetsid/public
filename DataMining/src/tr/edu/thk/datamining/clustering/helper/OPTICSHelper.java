package tr.edu.thk.datamining.clustering.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;
import tr.edu.thk.datamining.optics.model.Point;

public class OPTICSHelper extends ClusteringHelper {
	
	private PriorityQueue<Point>	seeds;

	private int						progressValue	= 0;
	
	public OPTICSHelper(List<Point> points, Point startingPoint, int epsilon, int minPts) {
		super(points, startingPoint, epsilon, minPts);
	}
	
	private void expandClusterOrder(Point currentPoint) {
		List<Point> neigbors = getNeighborPoints(points, currentPoint);
		write(currentPoint);
		
		Double coreDistance = computeCoreDistance(currentPoint);
		currentPoint.setCoreDistance(coreDistance);
		
		if(currentPoint.getCoreDistance() != null) {
			update(neigbors, currentPoint);
			
			while(!seeds.isEmpty()) {
				Point nextPoint = seeds.poll();
				List<Point> nextNeighbors = getNeighborPoints(points, nextPoint);
				
				write(nextPoint);
				
				coreDistance = computeCoreDistance(nextPoint);
				nextPoint.setCoreDistance(coreDistance);
				
				if(nextPoint.getCoreDistance() != null) {
					update(nextNeighbors, nextPoint);
				}
			}
		}
	}
	
	private void write(Point point) {
		if(!point.isProcessed()) {
			point.setProcessed(true);
			reachabilityPlot.add(point);
			
			progressValue++;
			setProgress(getProgressPercentage());
		}
	}
	
	private void update(List<Point> neigbors, Point currentPoint) {
		for(Point point : neigbors) {
			if(!point.isProcessed() || (point.getReachabilityDistance() == null)) {
				Double coreDistance = computeCoreDistance(point);
				point.setCoreDistance(coreDistance);
				double reachabilityDistance = Math.max(point.getCoreDistance(), currentPoint.distance(point));
				
				if(point.getReachabilityDistance() == null) {
					point.setReachabilityDistance(reachabilityDistance);
					seeds.add(point);
					
				} else if(reachabilityDistance < point.getReachabilityDistance()) {
					seeds.remove(point);
					point.setReachabilityDistance(reachabilityDistance);
					seeds.add(point);
				}
				
				point.setType(PointType.CORE);
			}
		}
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		prepareForClustering();
		seeds = new PriorityQueue<Point>();
		reachabilityPlot = new ArrayList<Point>();
		
		for(int pointIndex = 0; pointIndex < points.size(); pointIndex++) {
			Point point = points.get(pointIndex);
			
			if(point.isProcessed()) {
				continue;
			}
			
			expandClusterOrder(point);
			progressValue++;
			setProgress(getProgressPercentage());
		}
		
		return null;
	}
	
	@Override
	protected void done() {
		super.done();
		mainFrameListener.showGraphicalRepresentation();
	}
	
	private int getProgressPercentage() {
		double percent = (double) 100 * progressValue / points.size();
		return Util.round2Int(percent);
	}
	
	
//	public void process() {
//		prepareForClustering();
//		List<Point> pts = new ArrayList<Point>();
//		pts.addAll(points);
//
//		for (int index = 0; index < points.size(); index++) {
//			Point currentPoint = points.get(index);
//			Comparator<Point> distanceComparator = new PointComparator(PointComparisonType.DISTANCE, currentPoint);
//			Collections.sort(pts, distanceComparator);
//
//			double eps = pts.get(minPts).getDistance();
//			currentPoint.setCoreDistance(eps);
//			currentPoint.setReachibilityDistance(Util.INFINITY);
//		}
//
//		Comparator<Point> coreDistanceComparator = new PointComparator(PointComparisonType.CORE_DISTANCE);
//		Collections.sort(points, coreDistanceComparator);
//		applyEpsilonThreshold();
//	}
}
