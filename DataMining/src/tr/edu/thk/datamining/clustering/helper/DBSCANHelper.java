package tr.edu.thk.datamining.clustering.helper;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JProgressBar;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;
import tr.edu.thk.datamining.optics.model.Point;

public class DBSCANHelper extends ClusteringHelper {
	private Comparator<Point>	pointComparator	= new Comparator<Point>() {
		@Override
		public int compare(Point o1, Point o2) {
			double d1 = o1.distance(startingPoint);
			double d2 = o2.distance(startingPoint);

			if (Util.equal(d1, d2)) {
				return 0;
			} else if (d1 < d2) {
				return -1;
			} else {
				return 1;
			}
		}
	};	
	
	public DBSCANHelper(List<Point> points, Point startingPoint, int epsilon, int minPts) {
		super(points, startingPoint, epsilon, minPts);
	}
	
	private void applyEpsilonThreshold() {
		for (int index = 0; index < points.size(); index++) {
			Point point = points.get(index);
			int n = getNeighborPoints(points, point).size();

			if (n >= minPts) {
				point.setType(PointType.CORE);
			}
		}

		for (int index = 0; index < points.size(); index++) {
			Point point = points.get(index);

			if (point.getType() != PointType.CORE) {
				continue;
			}

			List<Point> neighborPoints = getNeighborPoints(points, point);
			for (Point neighborPoint : neighborPoints) {
				if (neighborPoint.getType() != PointType.CORE) {
					neighborPoint.setType(PointType.BORDER);
				}
			}
		}
	}
	
	
	@Override
	protected Void doInBackground() throws Exception {
		prepareForClustering();
		Collections.sort(points, pointComparator);
		applyEpsilonThreshold();
		return null;
	}
	
	@Override
	protected void prepareForClustering() {
		super.prepareForClustering();
		
		for(Point point : points) {
			double distance = point.distance(startingPoint);
		}
	}
}
