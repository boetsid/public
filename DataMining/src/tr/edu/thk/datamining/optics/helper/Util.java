package tr.edu.thk.datamining.optics.helper;

import java.awt.Color;
import java.awt.SystemColor;
import java.util.Random;

import tr.edu.thk.datamining.optics.model.Point;
import tr.edu.thk.datamining.optics.ui.Toast;

public class Util {
	private static final Random	RND					= new Random();

	public static final Color	BACKGROUND_COLOR	= SystemColor.inactiveCaption;

	public enum PointComparisonType {
		DISTANCE,
		CORE_DISTANCE,
		REACHABILITY_DISTANCE
	};
	
	public enum MainFrameMode {
		DRAW_CLUSTER_BOUNDARY,
		APPLY_OPTICS_ALGORITHM,
		NOTHING
	};

	public enum PointType {
		CORE(Color.yellow, 4),
		BORDER(Color.red, 4),
		UNDEFINED(Color.black, 4),
		CLUSTER(Color.blue, 4),
		STARTING_POINT(Color.green, 12),
		OUTLIER(Color.black, 4);

		private Color	color;
		private int		radius;

		private PointType(Color color, int radius) {
			this.color = color;
			this.radius = radius;
		}

		public Color getColor() {
			return color;
		}
		
		public int getRadius() {
			return radius;
		}
	};

	public static Point convert2Point(java.awt.Point point) {
		return new Point(point.x, point.y);
	}

	public static java.awt.Point convert2Point(Point point) {
		return new java.awt.Point(round2Int(point.x), round2Int(point.y));
	}

	public static int round2Int(double d) {
		return (int) Math.round(d);
	}

	public static double round(double d) {
		return round(d, 3);
	}

	public static double round(double d, int digits) {
		double coeff = Math.pow(10, digits);
		return (double) Math.round(d * coeff) / coeff;
	}

	public static boolean equal(double d1, double d2) {
		return round(d1) == round(d2);
	}

	public static void toast(String text) {
		Toast toast = new Toast(text, 500);
		toast.setModal(true);
		toast.setVisible(true);
	}
	
	public static Random getRnd() {
		return RND;
	}
	
	public static void wait(final int ms) {
		Thread t = new Thread() {
			public void run() {
				try {
					Thread.sleep(ms);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		t.start();
	}
}
