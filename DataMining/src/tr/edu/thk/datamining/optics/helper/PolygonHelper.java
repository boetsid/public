package tr.edu.thk.datamining.optics.helper;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import tr.edu.thk.datamining.optics.model.Line;
import tr.edu.thk.datamining.optics.model.Point;

public class PolygonHelper {
	public static Polygon createPolygon(List<Point> points) {
		if (!controlPolygon(points)) {
			return null;
		}

		List<Line> polygonLines = new ArrayList<Line>();
		
		List<Line> lines = buildLines(points);
		Line lastLine = lines.get(lines.size() - 1);
		
		int intersectingLineIndex = -1;
		Point intersectionPoint = null;

		for (int index = 0; index < lines.size() - 2; index++) {
			Line line = lines.get(index);
			
			if(lastLine.intersectsLine(line)) {
				Point p = lastLine.intersectionPoint(line);
				
				if(p != null) {
					intersectionPoint = p;
					intersectingLineIndex = index;
					break;
				}
			}
		}
		
		if(intersectionPoint == null) {
			return null;
		}
		
		for(int index = intersectingLineIndex; index < lines.size(); index++) {
			Line line = lines.get(index);
			
			if(index == 0) {
				line.setP1(intersectionPoint);
			} else if(index == lines.size() - 1) {
				line.setP2(intersectionPoint);
			}
			
			polygonLines.add(line);
		}
		
		int[] xs = new int[polygonLines.size() + 1];
		int[] ys = new int[polygonLines.size() + 1];

		for(int index = 0; index < polygonLines.size(); index++) {
			Line line = polygonLines.get(index);
			xs[index] = Util.round2Int(line.getP2().getX());
			ys[index] = Util.round2Int(line.getP2().getY());
		}
		
		xs[xs.length - 1] = xs[0];
		ys[ys.length - 1] = ys[0];
		
		Polygon polygon = new Polygon(xs, ys, xs.length);
		
		return polygon;
	}

	private static boolean controlPolygon(List<Point> points) {
		boolean polygon = false;

		if ((points == null) || (points.size() < 4)) {
			return polygon;
		}

		List<Line> lines = buildLines(points);
		Line lastLine = lines.get(lines.size() - 1);

		for (int index = 0; index < lines.size() - 2; index++) {
			Line line = lines.get(index);

			if (lastLine.intersectsLine(line)) {
				return true;
			}
		}

		return false;
	}

	private static List<Line> buildLines(List<Point> points) {
		ArrayList<Line> lines = new ArrayList<Line>();

		for (int index = 1; index < points.size(); index++) {
			Line line = new Line(points.get(index - 1), points.get(index));
			lines.add(line);
		}

		return lines;
	}
}
