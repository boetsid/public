package tr.edu.thk.datamining.optics.ui;

public interface MainPanelListener {
	public void showOriginalClusterBorders(boolean show);
	
	public void clearDataset();
	
	public void resetDataset();
	
	public void addOutlierPoints(int n);
	
	public void prepareForOptics(int epsilon, int minPts);
	
	public void showOutliers(boolean show);
}
