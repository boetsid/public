package tr.edu.thk.datamining.optics.ui;

import java.awt.Color;
import java.awt.SystemColor;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;
import tr.edu.thk.datamining.optics.model.Point;
import javax.swing.JProgressBar;

public class GraphicPanel extends JPanel {
	private List<Point>			reachabilityPlot;

	private XYSeriesCollection	dataset;

	private JFreeChart			chart;

	private ExChartPanel		chartPanel;

	private JSlider				slider;

	private static final int	SLIDER_LOWER_BOUND		= 187;
	private static final int	SLIDER_UPPER_BOUND		= 34;

	private static final int	SLIDER_INITIAL_VALUE	= 0;
	private static final int	SLIDER_MINIMUM			= 0;
	private static final int	SLIDER_MAXIMUM			= 234;

	private static final int	CHART_MINIMUM			= SLIDER_MAXIMUM - SLIDER_LOWER_BOUND;
	private static final int	CHART_MAXIMUM			= SLIDER_MAXIMUM - SLIDER_UPPER_BOUND;

	private java.lang.Double	maxReachabilityDistance	= 0.0;

	public GraphicPanel() {
		super();
		setOpaque(false);
		initGUI();
	}

	private void initGUI() {
		setLayout(new MigLayout("", "[fill][grow,fill]", "[][grow,fill]"));
		chartPanel = createChartPanel();
		add(chartPanel, "cell 1 1,grow");

		slider = new JSlider();
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				slider_stateChanged(e);
			}
		});
		slider.setOrientation(SwingConstants.VERTICAL);
		slider.setMinimum(SLIDER_MINIMUM);
		slider.setMaximum(SLIDER_MAXIMUM);
		slider.setValue(SLIDER_MAXIMUM);
		add(slider, "cell 0 1");

	}

	private void refresh() {
		createDataset();
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDataset(dataset);
	}

	private XYDataset createDataset() {
		final XYSeries series = new XYSeries("points");

		if (reachabilityPlot == null) {
			return null;
		}

		for (int index = 0; index < reachabilityPlot.size(); index++) {
			Point point = reachabilityPlot.get(index);
			Double reachabilityDistance = point.getReachabilityDistance();

			if (reachabilityDistance == null) {
				reachabilityDistance = 0d;
			}

			series.add(index, reachabilityDistance);
		}

		dataset = new XYSeriesCollection();
		dataset.addSeries(series);

		return dataset;
	}

	private JFreeChart createChart() {
		chart = ChartFactory.createXYLineChart("Reachability Plot", "point id", "Reachability Distance", createDataset(), PlotOrientation.VERTICAL, false,
				true, false);
		
		return chart;
	}

	private ExChartPanel createChartPanel() {
		if (chartPanel == null) {
			createChart();
			chartPanel = new ExChartPanel(chart);
		}

		return chartPanel;
	}

	public void setReachabilityPlot(List<Point> reachabilityPlot) {
		this.reachabilityPlot = reachabilityPlot;
		arrangeData();
		refresh();
	}

	private void arrangeData() {
		int size = reachabilityPlot.size();
		for (int index = size - 1; index >= 0; index--) {
			Point point = reachabilityPlot.get(index);
			if (point.getReachabilityDistance() == null) {
				reachabilityPlot.remove(index);

			} else if (maxReachabilityDistance < point.getReachabilityDistance()) {
				maxReachabilityDistance = point.getReachabilityDistance();
			}
		}
	}

	public ExChartPanel getChartPanel() {
		return chartPanel;
	}

	private void setPointTypes() {
		if(reachabilityPlot == null) {
			return;
		}
		
		java.lang.Double threshold = getEpsilonThreshold();
		
		if(threshold == null) {
			return;
		}
		
		for (Point point : reachabilityPlot) {
			if (point.getReachabilityDistance() <= threshold) {
				point.setType(PointType.CORE);
			} else {
				point.setType(PointType.UNDEFINED);
			}
		}

		getRootPane().updateUI();
	}

	private java.lang.Double getEpsilonThreshold() {
		int range = CHART_MAXIMUM - CHART_MINIMUM;
		double step = (double) (slider.getValue() - CHART_MINIMUM) / range;
		double value = step * maxReachabilityDistance;

		return value;
	}

	private void slider_stateChanged(ChangeEvent e) {
		int thresholdLevel = -1;
		int value = slider.getValue();

		boolean controlRange = (value >= CHART_MINIMUM) && (value <= CHART_MAXIMUM);

		if (!controlRange) {
			thresholdLevel = -1;
		} else {
			thresholdLevel = SLIDER_MAXIMUM - value;
		}

		if (chartPanel != null) {
			chartPanel.setThresholdLevel(thresholdLevel);
			setPointTypes();
		}
	}
	
	public void clear() {
		if(reachabilityPlot != null) {
			reachabilityPlot.clear();
		}
		refresh();
	}
}
