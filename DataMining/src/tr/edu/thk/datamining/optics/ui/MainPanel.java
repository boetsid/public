package tr.edu.thk.datamining.optics.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import tr.edu.thk.datamining.clustering.helper.ClusteringHelper;
import tr.edu.thk.datamining.optics.helper.PolygonHelper;
import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.MainFrameMode;
import tr.edu.thk.datamining.optics.helper.Util.PointType;
import tr.edu.thk.datamining.optics.model.Cluster;
import tr.edu.thk.datamining.optics.model.MainPanelModel;
import tr.edu.thk.datamining.optics.model.Point;

public class MainPanel extends JPanel implements MouseMotionListener, MouseListener, MainPanelListener {
	private MainPanelModel		model;

	private MainFrameMode		mode			= MainFrameMode.NOTHING;

	private boolean				pressed			= false;

	private Point				currentPoint;

	private MainFrameListener	mainFrameListener;

	public MainPanel() {
		setOpaque(false);
		setBackground(Color.GREEN);
		model = new MainPanelModel();
		setDoubleBuffered(true);

		initGUI();
	}

	private void initGUI() {
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(2.0f));

		for (Cluster cluster : model.getClusters()) {
			paintCluster(g2, cluster);
		}

		paintCluster(g2, model.getOutliers());
		drawMovingLine(g2);
	}

	private void drawMovingLine(Graphics g2) {
		Color color = g2.getColor();

		if (mode == MainFrameMode.DRAW_CLUSTER_BOUNDARY) {
			if ((model.getOpenCluster() != null) && !model.getOpenCluster().isEmpty()) {
				g2.setColor(model.getOpenCluster().getLineColor());
				java.awt.Point p = Util.convert2Point(currentPoint);
				java.awt.Point start = Util.convert2Point(model.getOpenCluster().getLastBoundaryPoint());
				g2.drawLine(start.x, start.y, p.x, p.y);
				g2.setColor(color);
			}
		}
	}

	private void paintCluster(Graphics2D g2, Cluster cluster) {
		Color color = g2.getColor();

		if (cluster.areLinesVisible()) {
			g2.setColor(cluster.getLineColor());

			int size = cluster.getBoundaryPoints().size();

			if (size > 0) {
				java.awt.Point start = Util.convert2Point(cluster.getBoundaryPoints().get(0));

				if (cluster.getBoundaryPoints().size() > 1) {
					for (int index = 1; index < cluster.getBoundaryPoints().size(); index++) {
						java.awt.Point point = Util.convert2Point(cluster.getBoundaryPoints().get(index));
						g2.drawLine(start.x, start.y, point.x, point.y);
						start = point;
					}
				} else {
					g2.fillOval(start.x - 1, start.y - 1, 3, 3);
				}
			}
		}

		drawClusterPoints(g2, cluster);
		g2.setColor(color);
	}

	private void drawClusterPoints(Graphics2D g2, Cluster cluster) {
		for (Point p : cluster.getPoints()) {
			if (!p.isVisible()) {
				continue;
			}

			java.awt.Point point = Util.convert2Point(p);
			int x = point.x - p.getRadius();
			int y = point.y - p.getRadius();
			int diameter = 2 * p.getRadius();

			g2.setColor(p.getColor());
			g2.fillOval(x, y, diameter, diameter);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (mode == MainFrameMode.DRAW_CLUSTER_BOUNDARY) {
			currentPoint = Util.convert2Point(e.getPoint());
			getRootPane().updateUI();
		}
	}

	public void setMode(MainFrameMode mode) {
		this.mode = mode;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if ((e.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) {
			pressed = true;
		} else {
			pressed = false;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if ((e.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) {
			if (model.getOpenCluster() != null) {
				if (PolygonHelper.createPolygon(model.getOpenCluster().getBoundaryPoints()) == null) {
					model.removeOpenCluster();
				}
			}
		}

		if ((e.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) {
			if (mode == MainFrameMode.DRAW_CLUSTER_BOUNDARY) {
				if (pressed) {
					if (model.getOpenCluster() == null) {
						model.getClusters().add(new Cluster());
					}

					Cluster cluster = model.getOpenCluster();
					Point point = new Point(e.getPoint().x, e.getPoint().y);
					cluster.addBoundaryPoint(point);

					Polygon polygon = PolygonHelper.createPolygon(cluster.getBoundaryPoints());
					if (polygon != null) {
						fillBoundaryPoints(cluster, polygon);
						cluster.setClosed(true);
						getRootPane().updateUI();
						requestForNumberOfPoints(cluster);
					}
					pressed = false;
				}
			}
		}

		getRootPane().updateUI();
	}

	private void requestForNumberOfPoints(Cluster cluster) {
		String s = JOptionPane.showInputDialog(getRootPane(), "Number of Points ?", "10");

		try {
			int n = Integer.parseInt(s);
			cluster.generatePoints(n);
		} catch (Exception e) {
		}
	}

	private void fillBoundaryPoints(Cluster cluster, Polygon polygon) {
		cluster.getBoundaryPoints().clear();

		int[] xs = polygon.xpoints;
		int[] ys = polygon.ypoints;

		for (int index = 0; index < xs.length; index++) {
			Point point = new Point(xs[index], ys[index]);
			cluster.addBoundaryPoint(point);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showOriginalClusterBorders(boolean show) {
		for (Cluster cluster : model.getClusters()) {
			cluster.setLinesVisible(show);
		}

		getRootPane().updateUI();
	}

	@Override
	public void clearDataset() {
		model.clear();
		getRootPane().updateUI();
	}

	@Override
	public void resetDataset() {
		model.reset();
		getRootPane().updateUI();
	}

	@Override
	public void addOutlierPoints(int n) {
		model.getOutliers().getPoints().clear();

		while (n > 0) {
			int x = Util.getRnd().nextInt(getWidth());
			int y = Util.getRnd().nextInt(getHeight());
			Point p = new Point(x, y);
			p.setType(PointType.OUTLIER);
			boolean outsideOfClusters = true;

			for (Cluster cluster : model.getClusters()) {
				outsideOfClusters &= !cluster.getPolygon().contains(p);
			}

			if (outsideOfClusters) {
				model.getOutliers().addPoint(p);
				n--;
			}
		}

		getRootPane().updateUI();
	}

	@Override
	public void prepareForOptics(int epsilon, int minPts) {
		model.preparereForClustering(epsilon, minPts);
		getRootPane().updateUI();
		
		model.computeStartingPoint();
		ClusteringHelper clusteringHelper = model.getClusteringHelper();
		clusteringHelper.setMainFrameListener(mainFrameListener);
		clusteringHelper.addPropertyChangeListener(mainFrameListener);
		clusteringHelper.execute();
		
		getRootPane().updateUI();
	}

	@Override
	public void showOutliers(boolean show) {
		model.showOutliers(show);
		getRootPane().updateUI();
	}
	
	public MainPanelModel getModel() {
		return model;
	}
	
	public void setMainFrameListener(MainFrameListener mainFrameListener) {
		this.mainFrameListener = mainFrameListener;
	}
}
