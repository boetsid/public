package tr.edu.thk.datamining.optics.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;
import tr.edu.thk.datamining.optics.helper.Util.MainFrameMode;

public class MenuPanel extends JPanel {
	private MainFrameListener	mainFrameListener;

	private MainPanelListener	mainPanelListener;
	private JTextField			txtEpsilon;
	private JTextField			txtMinPts;

	private JToggleButton		tglbtnShowClusterBorders;
	private JToggleButton		btnDrawClusterBoundary;
	private JToggleButton		tglBtnEliminateOutliers;

	public MenuPanel() {
		setBackground(new Color(70, 130, 180));
		initGUI();
	}

	private void initGUI() {
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		createButtons();
	}

	private void createButtons() {
		btnDrawClusterBoundary = new JToggleButton("Draw Clusters");
		btnDrawClusterBoundary.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnDrawClusterBoundary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDrawClusterBoundary_actionPerformed(e);
			}
		});
		JButton btnAddOutlierPoints = new JButton("Add Outlier Points");
		btnAddOutlierPoints.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnAddOutlierPoints.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCreatePoints_actionPerformed(e);
			}
		});
		setLayout(new MigLayout("", "[grow,fill]", "[30,fill][40,fill][40,fill][40,fill][40][30,fill][60,fill][40,fill][40,fill][40,fill][40,fill]"));

		JLabel lblControlPanel = new JLabel("Control Panel");
		lblControlPanel.setOpaque(true);
		lblControlPanel.setBackground(new Color(0, 139, 139));
		lblControlPanel.setForeground(SystemColor.text);
		lblControlPanel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblControlPanel.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblControlPanel, "cell 0 0,alignx center,aligny center");

		add(btnDrawClusterBoundary, "cell 0 1,alignx right,aligny center");
		add(btnAddOutlierPoints, "cell 0 2,alignx left,aligny center");

		JButton btnResetDataSet = new JButton("Reset Data Set");
		btnResetDataSet.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnResetDataSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnResetDataSet_actionPerformed(e);
			}
		});
		add(btnResetDataSet, "cell 0 3");

		JButton tglbtnClearDataset = new JButton("Clear Data Set");
		tglbtnClearDataset.setFont(new Font("Tahoma", Font.BOLD, 12));
		tglbtnClearDataset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tglbtnClearDataset_actionPerformed(e);
			}
		});
		add(tglbtnClearDataset, "cell 0 4,grow");

		JSeparator separator = new JSeparator();
		add(separator, "cell 0 5,growx");

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		add(panel, "cell 0 6,grow");
		panel.setLayout(new MigLayout("", "[120,fill][grow][]", "[24,fill][24,fill]"));

		JLabel lblNewLabel = new JLabel("Epsilon(\u03b5)");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(lblNewLabel, "cell 0 0,alignx trailing,growy");

		txtEpsilon = new JTextField();
		txtEpsilon.setText("40");
		txtEpsilon.setHorizontalAlignment(SwingConstants.RIGHT);
		txtEpsilon.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(txtEpsilon, "cell 1 0,growx");
		txtEpsilon.setColumns(10);

		JLabel lblPx = new JLabel("px");
		lblPx.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(lblPx, "cell 2 0");

		JLabel lblMinpts = new JLabel("MinPts");
		lblMinpts.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(lblMinpts, "cell 0 1");

		txtMinPts = new JTextField();
		txtMinPts.setText("3");
		txtMinPts.setHorizontalAlignment(SwingConstants.RIGHT);
		txtMinPts.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtMinPts.setColumns(10);
		panel.add(txtMinPts, "cell 1 1,growx");

		JButton tglbtnApplyOpticsAlgorithm = new JButton("Apply OPTICS Algorithm");
		tglbtnApplyOpticsAlgorithm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tglbtnApplyOpticsAlgorithm_actionPerformed(e);
			}
		});
		tglbtnApplyOpticsAlgorithm.setFont(new Font("Tahoma", Font.BOLD, 12));
		add(tglbtnApplyOpticsAlgorithm, "cell 0 7");

		tglbtnShowClusterBorders = new JToggleButton("Show Original Clusters");
		tglbtnShowClusterBorders.setFont(new Font("Tahoma", Font.BOLD, 12));
		tglbtnShowClusterBorders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tglbtnShowClusterBorders_actionPerformed(e);
			}
		});

		tglBtnEliminateOutliers = new JToggleButton("Show Outliers");
		tglBtnEliminateOutliers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEliminateOutliers_actionPerformed(e);
			}
		});
		tglBtnEliminateOutliers.setFont(new Font("Tahoma", Font.BOLD, 12));
		add(tglBtnEliminateOutliers, "cell 0 8,alignx center,aligny center");
		add(tglbtnShowClusterBorders, "flowy,cell 0 9");
	}

	private void requestForNumberOfOutliers() {
		String s = JOptionPane.showInputDialog(getRootPane(), "Number of Outlier Points ?", "10");

		try {
			int n = Integer.parseInt(s);
			mainPanelListener.addOutlierPoints(n);
		} catch (Exception e) {
		}
	}

	private void btnDrawClusterBoundary_actionPerformed(ActionEvent e) {
		JToggleButton button = (JToggleButton) e.getSource();

		if (button.isSelected()) {
			if (!tglbtnShowClusterBorders.isSelected()) {
				tglbtnShowClusterBorders.doClick();
			}
			
			if(tglBtnEliminateOutliers.isSelected()) {
				tglBtnEliminateOutliers.doClick();
			}
			
			mainFrameListener.setMainFrameMode(MainFrameMode.DRAW_CLUSTER_BOUNDARY);
		} else {
			mainFrameListener.setMainFrameMode(MainFrameMode.NOTHING);
		}
	}

	public void setMainFrameListener(MainFrameListener listener) {
		this.mainFrameListener = listener;
	}

	public void setMainPanelListener(MainPanelListener mainPanelListener) {
		this.mainPanelListener = mainPanelListener;
	}

	private void btnCreatePoints_actionPerformed(ActionEvent e) {
		requestForNumberOfOutliers();
	}

	protected void tglbtnShowClusterBorders_actionPerformed(ActionEvent e) {
		JToggleButton button = (JToggleButton) e.getSource();
		mainPanelListener.showOriginalClusterBorders(button.isSelected());
	}

	protected void tglbtnClearDataset_actionPerformed(ActionEvent e) {
		mainPanelListener.clearDataset();
		mainFrameListener.clearReachabilityPlot();
	}

	protected void btnResetDataSet_actionPerformed(ActionEvent e) {
		mainPanelListener.resetDataset();
		mainFrameListener.clearReachabilityPlot();
	}

	protected void tglbtnApplyOpticsAlgorithm_actionPerformed(ActionEvent e) {
		if (btnDrawClusterBoundary.isSelected()) {
			btnDrawClusterBoundary.doClick();
		}

		if (tglbtnShowClusterBorders.isSelected()) {
			tglbtnShowClusterBorders.doClick();
		}

		int epsilon = 20;
		int minPts = 4;

		mainFrameListener.setMainFrameMode(MainFrameMode.APPLY_OPTICS_ALGORITHM);
		mainFrameListener.initProgress();

		try {
			epsilon = Integer.parseInt(txtEpsilon.getText().trim());
			minPts = Integer.parseInt(txtMinPts.getText().trim());
		} catch (Exception e1) {
		}

		mainPanelListener.prepareForOptics(epsilon, minPts);
	}

	protected void btnEliminateOutliers_actionPerformed(ActionEvent e) {
		JToggleButton button = (JToggleButton) e.getSource();
		mainPanelListener.showOutliers(!button.isSelected());
	}
}
