package tr.edu.thk.datamining.optics.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

public class ExChartPanel extends ChartPanel {
	private JFreeChart	chart;
	
	private int thresholdLevel = -1;

	public ExChartPanel(JFreeChart chart) {
		super(chart);
		this.chart = chart;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics g2 = (Graphics2D) g;

		if(thresholdLevel > -1) {
			g2.drawLine(0, thresholdLevel, getWidth(), thresholdLevel);
		}
	}
	
	public void setThresholdLevel(int thresholdLevel) {
		this.thresholdLevel = thresholdLevel;
		repaint();
	}
}
