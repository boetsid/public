package tr.edu.thk.datamining.optics.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import net.miginfocom.swing.MigLayout;
import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.MainFrameMode;

public class MainFrame extends JFrame implements MainFrameListener {
	private MenuPanel				panelMenu;
	private MainPanel				panelMain;

	private MainFrameMode			mode			= MainFrameMode.NOTHING;

	private final JPanel			panel			= new JPanel();

	private final JSplitPane		splVertical		= new JSplitPane();

	private final GraphicPanel		graphicPanel	= new GraphicPanel();

	private final JProgressBar		progress		= new JProgressBar();
	
	public static void main(String[] args) {
		setNimbusLookAndFeel();
		MainFrame main = new MainFrame();
		main.setVisible(true);
	}

	public MainFrame() {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent e) {
				this_componentShown(e);
			}
		});
		setTitle("Data Mining Project - Burhan ÖZERDEM & Nezar AYAD");
		init();
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initGUI();
		setExtendedState(JFrame.MAXIMIZED_BOTH);
	}

	private void initGUI() {
		getContentPane().setLayout(new BorderLayout(0, 0));

		JSplitPane splitPane = new JSplitPane();
		splitPane.setOpaque(false);
		splitPane.setDividerSize(3);
		getContentPane().add(splitPane, BorderLayout.CENTER);

		panelMenu = new MenuPanel();
		panelMenu.setMainFrameListener(this);

		splitPane.setLeftComponent(panelMenu);
		splitPane.setDividerLocation(200);

		splVertical.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splVertical.setBackground(Util.BACKGROUND_COLOR);
		splitPane.setRightComponent(splVertical);
		splVertical.setLeftComponent(panel);
		panel.setBackground(Util.BACKGROUND_COLOR);
		panel.setLayout(new MigLayout("", "[grow,fill]", "[grow,fill][]"));

		panelMain = new MainPanel();
		panelMain.setMainFrameListener(this);
		panel.add(panelMain, "cell 0 0");
		panelMenu.setMainPanelListener(panelMain);

		panel.add(progress, "cell 0 1");

		splVertical.setRightComponent(graphicPanel);
	}

	private void fireModeChanged(MainFrameMode mode) {
		switch (mode) {
		case DRAW_CLUSTER_BOUNDARY:
			Cursor cursor = Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);
			panelMain.setCursor(cursor);
			break;

		case NOTHING:
			panelMain.setCursor(Cursor.getDefaultCursor());
		}

		this.mode = mode;
		panelMain.setMode(mode);
	}

	@Override
	public void setMainFrameMode(MainFrameMode mode) {
		if (this.mode == mode) {
			fireModeChanged(MainFrameMode.NOTHING);
		} else {
			fireModeChanged(mode);
		}
	}

	private static void setNimbusLookAndFeel() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("nimbus".compareToIgnoreCase(info.getName()) == 0) {
					UIManager.setLookAndFeel(info.getClassName());
				}
			}
		} catch (Exception e) {
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void showGraphicalRepresentation() {
		graphicPanel.setReachabilityPlot(panelMain.getModel().getReachabilityPlot());
		progress.setValue(0);
	}

	protected void this_componentShown(ComponentEvent e) {
		splVertical.setDividerLocation(getHeight() - 300);
	}

	@Override
	public void initProgress() {
		progress.setMinimum(0);
		progress.setMaximum(panelMain.getModel().getPointCount());
		progress.setValue(0);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if("progress".equals(evt.getPropertyName())) {
			int progressValue = (Integer) evt.getNewValue();
			double percent = (double) (progress.getMaximum() - progress.getMinimum()) * progressValue / 100;
			progress.setValue(Util.round2Int(percent));
			getRootPane().updateUI();
		}
	}
	
	@Override
	public void clearReachabilityPlot() {
		graphicPanel.clear();
	}
}
