package tr.edu.thk.datamining.optics.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import net.miginfocom.swing.MigLayout;
import java.awt.SystemColor;

public class Toast extends JDialog {
	public Toast(String text, final int ms) {
		JLabel lblText = new JLabel("Select Any Point to Start !!!");
		lblText.setFont(new Font("Tahoma", Font.BOLD, 14));

		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.info);
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setLayout(new MigLayout("", "[grow,fill]", "[40,grow,fill]"));
		panel.add(lblText, "cell 0 0,grow");

		setContentPane(panel);

		setUndecorated(true);
		setAlwaysOnTop(true);
		setLocationRelativeTo(null);

		pack();

		Thread t = new Thread() {
			public void run() {
				try {
					Thread.sleep(ms);
					dispose();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		t.start();
	}
}
