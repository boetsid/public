package tr.edu.thk.datamining.optics.ui;

import java.beans.PropertyChangeListener;

import tr.edu.thk.datamining.optics.helper.Util.MainFrameMode;

public interface MainFrameListener extends PropertyChangeListener {
	public void setMainFrameMode(MainFrameMode mode);
	
	public void showGraphicalRepresentation();
	
	public void initProgress();
	
	public void clearReachabilityPlot();
}
