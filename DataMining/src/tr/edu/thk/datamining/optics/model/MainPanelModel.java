package tr.edu.thk.datamining.optics.model;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import tr.edu.thk.datamining.clustering.helper.ClusteringHelper;
import tr.edu.thk.datamining.clustering.helper.DBSCANHelper;
import tr.edu.thk.datamining.clustering.helper.OPTICSHelper;
import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;

public class MainPanelModel {
	private List<Cluster>		clusters;

	private Cluster				outliers;

	private int					epsilon;

	private int					minPts;

	private Point				startingPoint;

	private ClusteringHelper	clusteringHelper;

	public MainPanelModel() {
		clusters = new ArrayList<Cluster>();

		outliers = new Cluster();
		outliers.setLineColor(Color.black);
		outliers.setLinesVisible(false);
	}

	public void clear() {
		clusters.clear();

		if (outliers != null) {
			outliers.getPoints().clear();
		} else {
			outliers = new Cluster();
		}
	}

	public void reset() {
		List<Point> points = getAllPoints();
		
		for (Point point : points) {
			point.setProcessed(false);
			point.setType(PointType.UNDEFINED);
		}
	}

	public void removeOpenCluster() {
		Cluster cluster = getOpenCluster();
		clusters.remove(cluster);
	}

	public Cluster getOpenCluster() {
		for (Cluster cluster : clusters) {
			if (!cluster.isClosed()) {
				return cluster;
			}
		}

		return null;
	}

	public List<Cluster> getClusters() {
		return clusters;
	}

	public Cluster getOutliers() {
		return outliers;
	}

	public Point getClosestPointToCursorInEpsilonNeighborhood(java.awt.Point p) {
		int edge = 2 * epsilon;
		Rectangle roi = new Rectangle(p.x - epsilon, p.y - epsilon, edge, edge);

		List<Point> possiblePoints = new ArrayList<Point>();
		List<Point> points = getAllPoints();

		for (Point point : points) {
			if (roi.contains(point)) {
				possiblePoints.add(point);
			}
		}

		if (possiblePoints.isEmpty()) {
			return null;
		}

		Point closestPoint = null;
		double closestDistance = 0;

		for (Point point : possiblePoints) {
			Point source = Util.convert2Point(p);
			double distance = source.distance(point);

			if (closestPoint == null) {
				closestDistance = distance;
				closestPoint = point;

			} else if (distance < closestDistance) {
				closestDistance = distance;
				closestPoint = point;
			}
		}

		return closestPoint;
	}
	
	public int getPointCount() {
		return getAllPoints().size();
	}

	private List<Point> getAllPoints() {
		List<Point> points = new ArrayList<Point>();

		for (Cluster cluster : clusters) {
			points.addAll(cluster.getPoints());
		}

		points.addAll(outliers.getPoints());

		return points;
	}

	public void setEpsilon(int epsilon) {
		this.epsilon = epsilon;
	}

	public void setMinPts(int minPts) {
		this.minPts = minPts;
	}
	
	public void computeStartingPoint() {
		List<Point> points = getAllPoints();
		
		if(!points.isEmpty()) {
			startingPoint = points.get(0);
		}
	}
	
	public void computeStartingPoint(java.awt.Point p) {
		startingPoint = getClosestPointToCursorInEpsilonNeighborhood(p);
	}

	public void showOutliers(boolean show) {
		List<Point> points = getAllPoints();

		for (Point point : points) {
			if (point.getType() == PointType.UNDEFINED) {
				point.setVisible(show);
			}
		}
	}

	public void preparereForClustering(int epsilon, int minPts) {
		this.epsilon = epsilon;
		this.minPts = minPts;
	}
	
	public ClusteringHelper getClusteringHelper() {
		clusteringHelper = new OPTICSHelper(getAllPoints(), startingPoint, epsilon, minPts); 
		return clusteringHelper;
	}
	
	public List<Point> getReachabilityPlot() {
		if(clusteringHelper != null) {
			return clusteringHelper.getReachabilityPlot();
		}
		
		return null;
	}
}
