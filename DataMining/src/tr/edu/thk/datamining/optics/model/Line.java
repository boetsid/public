package tr.edu.thk.datamining.optics.model;

import java.awt.geom.Line2D;

import tr.edu.thk.datamining.optics.helper.Util;

public class Line extends Line2D.Double {
	
	public Line(Point p1, Point p2) {
		super(p1, p2);
	}
	
	public boolean isVertical() {
		return Util.equal(getX1(), getX2());
	}

	public boolean isHorizontal() {
		return (y1 == y2);
	}
	
	public boolean isParallelTo(Line line) {
		if(isVertical() && line.isVertical()) {
			return true;
		}
		
		return Util.equal(slope(), line.slope());
	}
	
	public java.lang.Double slope() {
		if(isVertical()) {
//			return null;
			double sign = Math.signum(y2 - y1);
			return sign * Integer.MAX_VALUE;
		}
		
		return (y2 - y1) / (x2 - x1);
	}
	
	public void setP1(Point p) {
		x1 = p.x;
		y1 = p.y;
	}
	
	public void setP2(Point p) {
		x2 = p.x;
		y2 = p.y;
	}
	
	public Point intersectionPoint(Line line) {
		double m1 = slope();
		double m2 = line.slope();
		
		double y21 = line.getY2();
		double x21 = line.getX2();
		
		double x = ((y21 - y1) + (m1*x1 - m2*x21))/(m1 - m2);
		double y = m1*(x - x1) + y1;
		
		double rx = Util.round(x);
		double ry = Util.round(y);
		
		Point point = new Point(rx, ry); 
		boolean contains = contains(point) || line.contains(point);
		
		if(contains) {
			return point;
		}
		
		return null;
	}
	
	public boolean contains(Point p) {
		Point point = new Point(getP2().getX(), getP2().getY());
		
		Line line = new Line(p, point);
		boolean slopeControl = Util.equal(slope(), line.slope());
		
		double minx = Math.min(x1, x2);
		double maxx = Math.max(x1, x2);
		
		double miny = Math.min(y1, y2);
		double maxy = Math.max(y1, y2);
		
		boolean xcontrol = (minx <= p.x) && (p.x <= maxx);
		boolean ycontrol = (miny <= p.y) && (p.y <= maxy);
		
		boolean contains = slopeControl && xcontrol && ycontrol;
		
		return contains;
	}
}
