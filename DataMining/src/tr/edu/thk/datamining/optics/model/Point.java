package tr.edu.thk.datamining.optics.model;

import java.awt.Color;
import java.awt.geom.Point2D;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;

public class Point extends Point2D.Double implements Comparable<Point> {
	private PointType			type					= PointType.UNDEFINED;

	private Color				color					= PointType.UNDEFINED.getColor();

	private int					radius					= PointType.UNDEFINED.getRadius();
	
	private java.lang.Double	coreDistance			= null;

	private java.lang.Double	reachabilityDistance	= null;

	private boolean				visible					= true;

	private boolean				processed				= false;

	public Point(double x, double y) {
		super(x, y);
	}

	public void setType(PointType type) {
		this.type = type;
		this.color = type.getColor();
		this.radius = type.getRadius();
	}

	public PointType getType() {
		return type;
	}

	public Color getColor() {
		return color;
	}

	public int getRadius() {
		return radius;
	}

	public double distance(Point p) {
		// euclidean
		double xsq = Math.pow(p.x - x, 2);
		double ysq = Math.pow(p.y - y, 2);
		return Math.sqrt(xsq + ysq);

		// manhattan
		// double dist = Math.abs(p.x - x) + Math.abs(p.y - y);
		// return dist;
	}

	@Override
	public int compareTo(Point point) {
		if (reachabilityDistance > point.getReachabilityDistance()) {
			return 1;
		} else if (reachabilityDistance < point.getReachabilityDistance()) {
			return -1;
		}

		return 0;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setReachabilityDistance(java.lang.Double reachabilityDistance) {
		this.reachabilityDistance = reachabilityDistance;
	}

	public java.lang.Double getReachabilityDistance() {
		return reachabilityDistance;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public boolean isProcessed() {
		return processed;
	}
	
	public void setCoreDistance(java.lang.Double coreDistance) {
		this.coreDistance = coreDistance;
	}
	
	public java.lang.Double getCoreDistance() {
		return coreDistance;
	}
}
