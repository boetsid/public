package tr.edu.thk.datamining.optics.model;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tr.edu.thk.datamining.optics.helper.Util;
import tr.edu.thk.datamining.optics.helper.Util.PointType;

public class Cluster implements Serializable {
	private List<Point>			boundaryPoints;

	private List<Point>			points;

	private Color				lineColor		= Color.red;

	private boolean				linesVisible	= true;

	private boolean				closed			= false;

	public Cluster() {
		boundaryPoints = new ArrayList<Point>();
		points = new ArrayList<Point>();
	}
	
	public Polygon getPolygon() {
		Polygon polygon = new Polygon();

		for (int index = 0; index < boundaryPoints.size() - 1; index++) {
			Point p = boundaryPoints.get(index);
			java.awt.Point point = Util.convert2Point(p);
			polygon.addPoint(point.x, point.y);
		}

		return polygon;
	}

	public void generatePoints(int n) {
		Polygon polygon = getPolygon();
		Rectangle bounds = polygon.getBounds();

		while (n > 0) {
			int x = (Util.getRnd().nextInt() % bounds.width) + 1 + bounds.x;
			int y = (Util.getRnd().nextInt() % bounds.height) + 1 + bounds.y;
			Point p = new Point(x, y);
			p.setType(PointType.CLUSTER);

			if (polygon.contains(p)) {
				points.add(p);
				n--;
			}
		}
	}

	public Point getLastBoundaryPoint() {
		if (!boundaryPoints.isEmpty()) {
			return boundaryPoints.get(boundaryPoints.size() - 1);
		}

		return null;
	}

	public void addBoundaryPoint(Point point) {
		boundaryPoints.add(point);
	}

	public void addPoint(Point point) {
		points.add(point);
	}

	public boolean isEmpty() {
		return boundaryPoints.isEmpty();
	}

	public List<Point> getBoundaryPoints() {
		return boundaryPoints;
	}

	public void setBoundaryPoints(List<Point> boundaryPoints) {
		this.boundaryPoints = boundaryPoints;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
	}

	public void setLinesVisible(boolean linesVisible) {
		this.linesVisible = linesVisible;
	}

	public boolean areLinesVisible() {
		return linesVisible;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}
}
